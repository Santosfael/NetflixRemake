package br.com.rafael.netflixremake.Model;

import java.util.List;

public class MovieDetail {

    private final Movie movie;
    private final List<Movie> movies;

    public MovieDetail(Movie movie, List<Movie> movies) {
        this.movie = movie;
        this.movies = movies;
    }

    public Movie getMovie() {
        return movie;
    }

    public List<Movie> getMovies() {
        return movies;
    }
}
